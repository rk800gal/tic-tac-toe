Started on: 2/05/2021.

References:
- Video's and Articles I studied to make this possible.
1) Reference Code 1:
https://www.youtube.com/watch?v=_WpQpLL49y0

2) Reference Code 2:
https://gist.github.com/jainsamyak/e31a89c5b9225f61a6ab5a477ceeba1f

==================================================================

Basically the code is made of:
- The main Source File
- Computer.h/Computer.cpp (responsible for Computer vs Player mode)
- Features.h/Features.cpp (responsible for Player vs Player mode)

==================================================================
- What can you do with this program?

 Choose between Player vs Player and Computer vs Player.
 Play alone or play with the computer (AI does not really have intelligence but it works!) 
For the Player vs Player mode, simply type a number as seen on the screen to place your markers.
For the Computer vs Player mode, somply type the X and Y coordinates as seen on the screen to place
your marker against the Computer's marker.

* Acknowledging the Bugs:
(TTдTT)
 I wanted to be able to add the time feature, it would have been easy to do that but whenever I typed in
 something too fast as the game kept looping, it would start to get glitchy and I do not want to break my code.
 Instead I worked on the menu. Even then, I could not complete the menu. You cannot restart or exit IN THE MIDDLE of a game for Comp vs Player, but you CAN do that in the player vs player.
 
==================================================================

* About the files:

-- Features.h/.cpp
I meant to name it "Player" for player mode but it was too late, basically this was where I put code
for the player vs player mode. The grid is different compared to Computer.h's as you can see.

-- Computer.h/.cpp
This is where I wrote the Computer vs Player mode code, it uses X and Y coordinates instead of a single character
unlike the Player vs Player mode.

==================================================================
