#include <iostream>
#include <stdlib.h>
#include "Features.h"
#include "Computer.h"
using namespace std;

// Base Ccode reference from  : https://www.youtube.com/watch?v=xwwl8TgkwgU&t=347s
// Followed all this.

// The board has 9 spaces. Like this:
// 1 | 2 | 3
// 4 | 5 | 6
// 7 | 8 | 9

// There are two modes

// Computer vs Player (made in Computer.h)
// Player vs Player (made in Features.h, accidentally named it features instead of players.)
// I think if you play the game too fast bugs will appear because it's slow, so I don't
// really want to implement a TIME feature (even though I could).

// The game menu for computer mode is different from player mode
// because it was easier that way.

// Reference code has been chopped up into classes so that this main function over here
// could happen. It's very messy but at least it works lol.
//if I had more time I would make it cleaner.

int main()
{
    Computer Game2;
    Features Game;
    int mode;
    cout << "Select a Mode: \n";
    cout << "Press 1 for Player vs Player\n";
    cout << "Press 2 for Computer vs Player\n";
    cin >> mode;

    switch (mode) {
    case 1:
        Game.drawBoard();
        Game.playerVplayer();
        break;
    case 2:
       Game2.computerVplayer();
        break;
    default:
        cout << "Select a valid number, please: \n";
        break;
    }

    system("pause");
   
}