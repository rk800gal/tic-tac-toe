#pragma once
#include<iostream>
#include <string>
#include <stdlib.h>

using namespace std;

constexpr auto GRID_SIZE = 3;
// referenced from https://github.com/PARAG00991/Tic-Tac-Toe/blob/master/tictactoe.c

// Trying to convert it into classes...
// This is a minimax algorithm

class Computer
{
public:

	void gameBoard(void);
	void initBoard(void);

	//---------------

	void computerMove(void);
	// computer's turn

	void playerMove(void);

	//------------------

	char checkWin(void);

	void computerVplayer(); // The mode I am constructing.

	//-----------------

	int playAgain(); // to play again..
	void menu2(); // made another menu because it works better that way.

};

