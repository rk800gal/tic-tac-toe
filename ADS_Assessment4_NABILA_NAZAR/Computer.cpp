#include "Computer.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Features.h"

//Reference Code: https://gist.github.com/jainsamyak/e31a89c5b9225f61a6ab5a477ceeba1f
// Converted all those functions into classes for me to use here in this header file.

using namespace std;

//Global Variables
char grid[3][3];


void Computer::gameBoard()
{
    system("cls");
    cout << "   A Tic Tac Toe Game!!!" << endl; // Title of the game.
    cout << "Player 1 (X)  -  Computer (O)" << endl << endl;
    cout << "Coordinates you can type in\n";
    cout << "==================\n";
    cout << "X, Y | X, Y | X, Y\n" << endl;
    cout << "1, 1 | 1, 2 | 1, 3\n";
    cout << "2, 1 | 2, 2 | 2, 3\n";
    cout << "3, 1 | 3, 2 | 3, 3\n";

    cout << endl;

    int t;

    for (t = 0; t < 3; t++) {

        cout << "\n    " << grid[t][0] << "     |    " << grid[t][1] << "     |    " << grid[t][2] << "\n";
        for (int j = 0; j < 3; j++)
        {
            cout << "___" << "_______|";
        }
        if (t != 2)
        {
            cout << "\n          |          |";
            cout << "\n          |          |";
        }
    }
    cout << "\n";

}

void Computer::initBoard(void)
{
    int i, j;

    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++) grid[i][j] = ' ';
}

void Computer::computerMove(void)
{
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++)
            if (grid[i][j] == ' ') break;
        if (grid[i][j] == ' ') break;
    }

    if (i * j == 9) {
        printf("draw\n");
        exit(0);
    }
    else
        grid[i][j] = 'O';
    
}

void Computer::playerMove(void)
{
    Features exitGame;
    int x, y;

    cout << "\nPlayer turn! You are X\n";
    cout << "________________________\n";
    cout << "Enter X Coordinate : " << endl;
    cin >> x;
    cout << "Enter Y Coodinate:" << endl;
    cin >> y;

    x--; y--;
 
    if (grid[x][y] != ' ') {
        cout << "\nSpace is filled/invalid !!\nPlease try AGAIN:\n";

        playerMove();
    }
    else grid[x][y] = 'X';
}

char Computer::checkWin(void)
{
    int i;

    for (i = 0; i < 3; i++)  /* check rows */
        if (grid[i][0] == grid[i][1] &&
            grid[i][0] == grid[i][2]) return grid[i][0];

    for (i = 0; i < 3; i++)  /* check columns */
        if (grid[0][i] == grid[1][i] &&
            grid[0][i] == grid[2][i]) return grid[0][i];

    /* test diagonals */
    if (grid[0][0] == grid[1][1] &&
        grid[1][1] == grid[2][2])
        return grid[0][0];

    if (grid[0][2] == grid[1][1] &&
        grid[1][1] == grid[2][0])
        return grid[0][2];

    return ' ';
}

void Computer::computerVplayer()
{
    char done;

    done = ' ';
   
    initBoard();

    do{
        gameBoard();
        playerMove();
        done = checkWin(); /* see if winner */
        if (done != ' ') break;
        computerMove();
        done = checkWin(); /* see if winner */
    } while (done == ' ');
    if (done == 'X') {
        gameBoard();
        cout << "\n You have beaten the dummy computer\n";
        playAgain();
    }
    else
        gameBoard();
        cout << "\nComputer has won! Oh no :(\n";
        playAgain();

    return;
}



int Computer::playAgain()
{
    Features matrix;
    char again;
    do {
        cout << "\nPlay again?\n> Y/y for yes (MENU)\n> N/n for no (TTERMINATE PROGRAM)" << endl;
        cin >> again;


    } while (again != 'Y' && again != 'y' && again != 'N' && again != 'n');

    if (again == 'Y' || again == 'y') {
        system("cls");
        matrix.clearMatrix(grid);
        menu2();
    }
    else if (again == 'N' || again == 'n')
    {
        cout << "Thanks for play <3\nHave a nice day!\n";
        exit(0);
    }
}

void Computer::menu2()
{
    Features playermode;
    int mode;
    cout << "Select a Mode: \n";
    cout << "Press 1 for Player vs Player\n";
    cout << "Press 2 for Computer vs Player\n";
    cin >> mode;

    switch (mode) {
    case 1:
    Features playermode;
        playermode.drawBoard();
        playermode.playerVplayer();
        break;
    case 2:
        computerVplayer();
        break;
    default:
        cout << "\nSelect a valid number, please: \n";
        break;
    }
}
