#include "Features.h"
#include <iostream>
#include "Computer.h"
using namespace std;

char matrix[3][3] = { {'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'} };
char player = 'X';
int n;

void Features::playerInput()
{

        int input;
        cout << "\nEnter a number in the field to place your move!\n" << player << "'s turn: " << endl;
        cin >> input;

        if (input == 0)
        {
            exiting();
        }


        if (input == 1)
        {
            if (matrix[0][0] == '1') // This will place the player's X in that desired spot.
                matrix[0][0] = player;
            else
            {
                cout << "Field is already filled in try again!" << endl;
                playerInput();
            }
        }


        else if (input == 2) // repeat for the rest of the board. (whew).
            if (matrix[0][1] == '2')
                matrix[0][1] = player;
            else
            {
                cout << "Field is already filled in try again!" << endl;
                playerInput();
            }

        else if (input == 3)
            if (matrix[0][2] == '3')
                matrix[0][2] = player;
            else
            {
                cout << "Field is already filled in try again!" << endl;
                playerInput();
            }

        else if (input == 4)
            if (matrix[1][0] == '4')
                matrix[1][0] = player;
            else
            {
                cout << "Field is already filled in try again!" << endl;
                playerInput();
            }

        else if (input == 5)
            if (matrix[1][1] == '5')
                matrix[1][1] = player;
            else
            {
                cout << "Field is already filled in try again!" << endl;
                playerInput();
            }


        else if (input == 6)
            if (matrix[1][2] == '6')
                matrix[1][2] = player;
            else
            {
                cout << "Field is already filled in try again!" << endl;
                playerInput();
            }
        else if (input == 7)
            if (matrix[2][0] == '7')
                matrix[2][0] = player;
            else
            {
                cout << "Field is already filled in try again!" << endl;
                playerInput();
            }

        else if (input == 8)
            if (matrix[2][1] == '8')
                matrix[2][1] = player;
            else
            {
                cout << "Field is already filled in try again!" << endl;
                playerInput();
            }


        else if (input == 9)
            if (matrix[2][2] == '9')
                matrix[2][2] = player;
            else
            {
                cout << "Field is already filled in try again!" << endl;
                playerInput();
            }
   
}

void Features::drawBoard()
{
    system("cls");
    cout << "   A Tic Tac Toe Game!!!" << endl; // Title of the game.
    cout << "Player 1 (X)  -  Player 2 (O)" << endl << endl;
    cout << endl;

    cout << "     |     |     \n";
    cout << "  " << matrix[0][0] << "  |  " << matrix[0][1] << "  |  " << matrix[0][2] << " \n";
    cout << "_____|_____|_____\n";
    cout << "     |     |     \n";
    cout << "  " << matrix[1][0] << "  |  " << matrix[1][1] << "  |  " << matrix[1][2] << " \n";
    cout << "_____|_____|_____\n";
    cout << "     |     |     \n";
    cout << "  " << matrix[2][0] << "  |  " << matrix[2][1] << "  |  " << matrix[2][2] << " \n";
    cout << "     |     |     \n";
    cout << "\n Press 0 to EXIT GAME anytime\n";
}

void Features::menu()
{
    Computer machineMode;
    int mode;
    cout << "Select a Mode: \n";
    cout << "Press 1 for Player vs Player\n";
    cout << "Press 2 for Computer vs Player\n";
    cin >> mode;

    switch (mode) {
    case 1:
        drawBoard();
        playerVplayer();
        break;
    case 2:
        machineMode.computerVplayer();
        break;
    default:
        cout << "Select a valid number, please: \n";
        break;
    }
}



void Features::playAgain()
{
    char again;
    do {
        cout << "Play again?\n> Y/y for yes\n> N/n for no" << endl;
        cin >> again;


    } while (again != 'Y' && again != 'y' && again != 'N' && again != 'n');

    if (again == 'Y' || again == 'y') {
        system("cls");
        clearMatrix(matrix);
        menu();
    }
    else if (again == 'N' || again == 'n')
    {
        cout << "Thank you for playing <3\n";
        cout << "Have a nice day (OuO)/\n";
        system("pause");
    }
}

char Features::Win()
{
    // Fix with a better board tomorrow.
//first player
    if (matrix[0][0] == 'X' && matrix[0][1] == 'X' && matrix[0][2] == 'X')
        return 'X';
    if (matrix[1][0] == 'X' && matrix[1][1] == 'X' && matrix[1][2] == 'X')
        return 'X';
    if (matrix[2][0] == 'X' && matrix[2][1] == 'X' && matrix[2][2] == 'X')
        return 'X';

    if (matrix[0][0] == 'X' && matrix[1][0] == 'X' && matrix[2][0] == 'X')
        return 'X';
    if (matrix[0][1] == 'X' && matrix[1][1] == 'X' && matrix[2][1] == 'X')
        return 'X';
    if (matrix[0][2] == 'X' && matrix[1][2] == 'X' && matrix[2][2] == 'X')
        return 'X';

    if (matrix[0][0] == 'X' && matrix[1][1] == 'X' && matrix[2][2] == 'X')
        return 'X';
    if (matrix[2][0] == 'X' && matrix[1][1] == 'X' && matrix[0][2] == 'X')
        return 'X';

    //second player
    if (matrix[0][0] == 'O' && matrix[0][1] == 'O' && matrix[0][2] == 'O')
        return 'O';
    if (matrix[1][0] == 'O' && matrix[1][1] == 'O' && matrix[1][2] == 'O')
        return 'O';
    if (matrix[2][0] == 'O' && matrix[2][1] == 'O' && matrix[2][2] == 'O')
        return 'O';

    if (matrix[0][0] == 'O' && matrix[1][0] == 'O' && matrix[2][0] == 'O')
        return 'O';
    if (matrix[0][1] == 'O' && matrix[1][1] == 'O' && matrix[2][1] == 'O')
        return 'O';
    if (matrix[0][2] == 'O' && matrix[1][2] == 'O' && matrix[2][2] == 'O')
        return 'O';

    if (matrix[0][0] == 'O' && matrix[1][1] == 'O' && matrix[2][2] == 'O')
        return 'O';
    if (matrix[2][0] == 'O' && matrix[1][1] == 'O' && matrix[0][2] == 'O')
        return 'O';

    return '/';
}



void Features::TogglePlayer()
{

    if (player == 'X')
        player = 'O';
    else
        player = 'X';

 
}


void Features::clearMatrix(char matrix[3][3])
{
    char h = '1';
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
        {
            matrix[i][j] = h;
            h++;
        }
}



void Features::playerVplayer()
{
    while (true)
    {
        n++;
        playerInput();
        drawBoard();
        if (Win() == 'X')
        {
            cout << "X wins!" << endl;
            playAgain();
        }
        else if (Win() == 'O')
        {
            cout << "O wins!" << endl;
            playAgain();
        }
        else if (Win() == '/' && n == 9) //if it's a draw and board's all filled up...
        {
            cout << "Whew, it's a draw..." << endl;
            playAgain();
        }
        TogglePlayer();
    }
}

void Features::exiting()
{
    char finish;
   
        cout << "\nAre you sure you want to exit?\n";
        cout << "[y]es to terminate program\n [n]ope to choose another mode\n";
        cin >> finish;

    switch (finish) {
    case 'n':
        system("cls");
        clearMatrix(matrix);
        menu();
        break;
    case 'y':
        system("cls");
        cout << "\nThank you for playing <3\n";
        cout << "Have a nice day (OuO)/\n";
        exit(0);
        break;
    default:
        cout << "Select a valid option, please: \n";
        break;
    }

}




