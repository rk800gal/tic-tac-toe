#pragma once
class Features
{
public:
	void playerInput();
	void drawBoard();
	void menu();

	void playAgain();
	char Win();

	void TogglePlayer();
	
	void clearMatrix(char matrix[3][3]); // to erase the board after you're done

	// Making the Computer v Player mode.. In another class cause that's a differeint game mode.
	void playerVplayer(); // this is the player vs player mode

	void exiting();
};

